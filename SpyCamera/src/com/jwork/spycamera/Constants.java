package com.jwork.spycamera;

public class Constants {

	public final static String PREFS_PREVIEW_SIZE = "previewSize";
	public static final String PREFS_AUTOSHOT_DELAY = "autoshotDelay";
	public static final String PREFS_SHOW_TOAST = "showToast";
	public static final String PREFS_IMAGE_SIZE = "imageSize-";
	public static final String PREFS_IMAGE_SIZE_0 = "imageSize-0";
	public static final String PREFS_IMAGE_SIZE_1 = "imageSize-1";
	public static final String PREFS_CAMERA_ID = "cameraId";
	public static final String PREFS_PREVIEW_ON_AUTOSHOT = "previewDisplayOnAutoCapture";
	public static final String PREFS_CRASHED = "justCrashed";
	public static final String PREFS_APP_VERSION = "appVersion";
	public static final String PREFS_HIDE_FOLDER = "hideFolder";
	public static final String PREFS_VIBRATION = "vibration";
	public static final String PREFS_CAMERA_PREVIEW_SIZES = "cameraPreviewSizes-";
	public static final String PREFS_ERRORREPORT_SETDISPLAYORIENTATION = "errorReportSetDisplayOrientation";
	
	public final static int TYPE_CAPTURE = 0;
	public final static int TYPE_AUTO = 1;
	public final static int TYPE_FACE = 2;

	public static final int VIBRATION_TIME = 100;
}
