/* ========================================================================
 * Copyright 2012 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.spycamera;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Face;
import android.hardware.Camera.FaceDetectionListener;
import android.hardware.Camera.OnZoomChangeListener;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.ShutterCallback;
import android.hardware.Camera.Size;
import android.media.AudioManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.OnScaleGestureListener;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * 
 * @author Jimmy
 *
 */
public class SpyCamActivity extends Activity implements OnClickListener, Callback, PreviewCallback
, AutoFocusCallback, Thread.UncaughtExceptionHandler, OnTouchListener, OnScaleGestureListener, OnZoomChangeListener, PictureCallback, ShutterCallback {

	//Constants
	private final static String TAG = SpyCamActivity.class.getSimpleName();
	private final static SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
	public static final int ACTIVITY_SETTING = 0;

	//UI Widget
	private Button btnAuto;
	private Button btnCapture;
	private Button btnSetting;
	private Button btnFace;
	private Button btnIncreaseSize;
	private Button btnDecreaseSize;
	private Button btnBlack;
	private SurfaceView svPreview;
	private SurfaceHolder shPreview;
	private LinearLayout blackLayout;

	//States
	private boolean isCamConfigure;
	private boolean isTakingPicture = false;
	private int typeCapture = 0;
	private boolean isInBlackState = false;
	private boolean isZoomErrorHaveDisplayed = false;
	private boolean zoomRunning = false;
	
	//Sound data
	private int volSystem;

	//Camera data
	private Camera camera;
	private byte[] bSnapShot;
	private Camera.Size previewSize;
	private Camera.Size previewSizeHighest;
	private Camera.Size captureSize;
	private Camera.Size captureSizeHighest;
	private boolean isImageHighRes = false;
	private Camera.Parameters cameraParameters;

	//Data
	private UncaughtExceptionHandler defaultUEH = null;
	private ScaleGestureDetector scaleGestureDetector;
	private SharedPreferences prefs;
	private long lastCapture = 0;
	private long lastStartAutofocus = 0;
	private long avgAutofocusTime = -1;
	private int cameraId;
	private int[] nonBlackState;
	private String[][] cameraPreviewSizes;
	private boolean crashReport = false;
	private int zoomCurrent = 0;
	private int defaultOrientation;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initConfig();
		defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
		crashReport = checkPreviousCrash();
		Thread.setDefaultUncaughtExceptionHandler(this);
		initView();
		if (!crashReport) {
			SpyCamActivity.showChangelogNew(true, this);
		}
	}

	public static void showChangelogNew(boolean check, final Activity activity) {
		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
		String version = prefs.getString(Constants.PREFS_APP_VERSION, "");
		if (!check || (check && !version.equals(activity.getString(R.string.app_versionName)))) {
			Editor editor = prefs.edit();
			editor.remove(Constants.PREFS_CAMERA_PREVIEW_SIZES + "0");
			editor.remove(Constants.PREFS_CAMERA_PREVIEW_SIZES + "1");
			editor.remove("imageSize");
			editor.commit();

			AlertDialog dialog = new AlertDialog.Builder(activity).create();
			dialog.setTitle("Changelog");

			WebView wv = new WebView(activity.getApplicationContext());
			wv.loadData(activity.getString(R.string.changelog_dialog_text), "text/html", "utf-8");
			wv.setScrollContainer(true);
			dialog.setView(wv);

			dialog.setButton(AlertDialog.BUTTON_NEGATIVE, activity.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Editor editor = prefs.edit();
					editor.putString(Constants.PREFS_APP_VERSION, activity.getString(R.string.app_versionName));
					editor.commit();
					dialog.dismiss();
				}
			});
			dialog.setButton(AlertDialog.BUTTON_POSITIVE, "Rate It", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Editor editor = prefs.edit();
					editor.putString(Constants.PREFS_APP_VERSION, activity.getString(R.string.app_versionName));
					editor.commit();
					dialog.dismiss();
					Uri uri = Uri.parse("market://details?id=" + activity.getPackageName());
					Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
					try {
						activity.startActivity(myAppLinkToMarket);
					} catch (ActivityNotFoundException e) {
						Toast.makeText(activity, "Failed to find Market application", Toast.LENGTH_LONG).show();
					}
				}
			});

			dialog.show();
		}
	}

	private void initConfig() {
		PreferenceManager.setDefaultValues(this, R.xml.setting, false);
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
	}

	private void initView() {
		setContentView(R.layout.activity_main);

		Log.i(TAG, "Default orientation : " + defaultOrientation);

		btnAuto = (Button)findViewById(R.id.btnAuto);
		btnAuto.setOnClickListener(this);
		btnCapture = (Button)findViewById(R.id.btnCapture);
		btnCapture.setOnClickListener(this);
		btnSetting = (Button)findViewById(R.id.btnSetting);
		btnSetting.setOnClickListener(this);
		btnIncreaseSize = (Button)findViewById(R.id.btnIncreseSize);
		btnIncreaseSize.setOnClickListener(this);
		btnDecreaseSize = (Button)findViewById(R.id.btnDecreaseSize);
		btnDecreaseSize.setOnClickListener(this);
		btnBlack = (Button)findViewById(R.id.btnBlack);
		btnBlack.setOnClickListener(this);
		btnFace = (Button)findViewById(R.id.btnFace);
		btnFace.setOnClickListener(this);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			btnFace.setVisibility(View.INVISIBLE);
		}

		scaleGestureDetector = new ScaleGestureDetector(this, this);

		svPreview = (SurfaceView)findViewById(R.id.svPreview);
		svPreview.setDrawingCacheQuality(100);
		svPreview.setDrawingCacheEnabled(true);
		ViewGroup.LayoutParams params = svPreview.getLayoutParams();
		params.width = prefs.getInt(Constants.PREFS_PREVIEW_SIZE, 250);
		params.height = params.width;
		svPreview.setLayoutParams(params);
		svPreview.setZOrderOnTop(true);
		svPreview.setOnTouchListener(this);

		shPreview = svPreview.getHolder();
		shPreview.addCallback(this);
		shPreview.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		shPreview.setFormat(PixelFormat.TRANSPARENT);

		blackLayout = (LinearLayout)findViewById(R.id.blackLayout);
		blackLayout.setVisibility(View.INVISIBLE);
	}

	@SuppressLint("NewApi")
	@Override
	protected void onResume() {
		Log.d(TAG, "onResume");
		super.onResume();
		if (!crashReport) {
			isCamConfigure = false;
			cameraId = 0;
			try {
				if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.FROYO) {
					camera = Camera.open();
					cameraPreviewSizes = new String[1][];
				} else {

					int total = Camera.getNumberOfCameras();
					if (total>2) {
						total = 2;
					}

					cameraPreviewSizes = new String[total][];

					String value = prefs.getString(Constants.PREFS_CAMERA_ID, "0");
					try {
						cameraId = Integer.parseInt(value);
					} catch (NumberFormatException e) {};

					if (total>1) {
						int anotherCameraId = (cameraId==0)?1:0;
						if (!prefs.contains(Constants.PREFS_CAMERA_PREVIEW_SIZES+anotherCameraId)) {
							camera = Camera.open(anotherCameraId);
							StringBuffer data = new StringBuffer();
							cameraPreviewSizes[anotherCameraId] = Utility.cameraSizeSupport(camera.getParameters(), data);
							camera.release();
							Editor editor = prefs.edit();
							editor.putString("cameraPreviewSizes-"+anotherCameraId, data.toString());
							Log.d(TAG, "cameraPreviewSizes-"+anotherCameraId+":"+data.toString());
							editor.commit();
						} else {
							cameraPreviewSizes[anotherCameraId]=prefs.getString(Constants.PREFS_CAMERA_PREVIEW_SIZES+anotherCameraId, "").split("#");
						}
					}

					camera = Camera.open(cameraId);
				}
				cameraParameters = camera.getParameters();

				if (!prefs.contains("cameraPreviewSizes-"+cameraId)) {
					// Get preview sizes
					StringBuffer data = new StringBuffer();
					cameraPreviewSizes[cameraId] = Utility.cameraSizeSupport(camera.getParameters(), data);
					Editor editor = prefs.edit();
					editor.putString(Constants.PREFS_CAMERA_PREVIEW_SIZES+cameraId, data.toString());
					Log.d(TAG, "cameraPreviewSizes-"+cameraId+":"+data.toString());
					editor.commit();
				} else {
					cameraPreviewSizes[cameraId]=prefs.getString(Constants.PREFS_CAMERA_PREVIEW_SIZES+cameraId, "").split("#");
				}

				String configSize = prefs.getString(Constants.PREFS_IMAGE_SIZE+cameraId, null);
				if (configSize!=null) {
					if (configSize.endsWith("*")) {
						isImageHighRes = true;
					} else {
						isImageHighRes = false;
					}
				} else {
					isImageHighRes = false;
				}

				// Analyze preview/capture size
				int highestPreview = 0;
				int highestCapture = 0;
				this.previewSize = null;
				this.previewSizeHighest = null;
				this.captureSize = null;
				this.captureSizeHighest = null;
				for (String previewSize : cameraPreviewSizes[cameraId]) {
					Log.d(TAG, "previewSize: " + previewSize);
					int w = 0;
					int h = 0;
					String size = null;
					if (previewSize.endsWith("*")) {
						size = previewSize.substring(0, previewSize.length()-1);
					} else {
						size = previewSize;
					}
					String[] temp = size.split("x");
					try {
						w = Integer.parseInt(temp[0]);
					} catch (NumberFormatException e) {}
					try {
						h = Integer.parseInt(temp[1]);
					} catch (NumberFormatException e) {}

					if (previewSize.endsWith("*") && (highestCapture<w*h)) {
						captureSizeHighest = camera.new Size(w, h);
						highestCapture = w*h;
					} else if (!previewSize.endsWith("*") && (highestPreview<w*h)) {
						previewSizeHighest = camera.new Size(w, h);
						highestPreview = w*h;
					}

					if (configSize!=null && previewSize.equals(configSize)) {
						if (isImageHighRes) {
							this.captureSize = camera.new Size(w, h);
							this.previewSize = null;
						} else {
							this.previewSize = camera.new Size(w, h);
							this.captureSize = null;
						}
					}
				}

				if (previewSize==null) {
					previewSize = previewSizeHighest;
				}

				if (isImageHighRes && this.captureSize==null) {
					this.captureSize = captureSizeHighest;
				}

				if (isImageHighRes) {
					SharedPreferences.Editor editor = prefs.edit();
					editor.putString(Constants.PREFS_IMAGE_SIZE+cameraId, captureSize.width+"x"+captureSize.height+"*");
					editor.commit();
				} else {
					SharedPreferences.Editor editor = prefs.edit();
					editor.putString(Constants.PREFS_IMAGE_SIZE+cameraId, previewSize.width+"x"+previewSize.height);
					editor.commit();
				}

				changePreviewSize(0);

				if (shPreview!=null) {
					try {
						camera.setPreviewDisplay(shPreview);
					} catch (IOException e) {
						Log.w(TAG, e);
					}
				}
//				camera.startPreview();
			} catch (RuntimeException re) {
				crashReport = true;
				if (re.getMessage().toLowerCase().contains("connect")) {
					clearPreference(false);
					showFailedProcess(re
							, "Failed initializing camera. Please try to reboot your phone manually and run the application again."
							, "After reboot still error"
							, "OK", true, null);
				} else if (re.getMessage().toLowerCase().contains("getparameters")) {
					clearPreference(false);
					showFailedProcess(re
							, "Failed getting camera parameters. Sending crash report will help me fix it."
							, "Send Report"
							, "I've sent it", true, null);
//				} else if (re.getMessage().toLowerCase().contains("startpreview")) {
//					showFailedProcess(re
//							, "Failed starting camera preview. Sending crash report will help me fix it."
//							, "Send Report"
//							, "I've sent it", false);
				} else {
					throw re;
				}
			}
		}
	}

	@Override
	protected void onPause() {
		Log.d(TAG, "onPause");
		super.onPause();
		if (!crashReport) {
			isCamConfigure = false;
			camera.cancelAutoFocus();
			camera.setPreviewCallback(null);
//			camera.stopPreview();
			try {
				camera.release();
				camera = null;
			} catch (Exception e) {
				Log.w(TAG, e);
			}
			showAllUI();
			typeCapture=Constants.TYPE_CAPTURE;
		}
	}

	@Override
	public void onClick(View v) {
		if (v==btnCapture) {
			capture();
		} else if (v==btnAuto) {
			startAutoCapture(Constants.TYPE_AUTO);
		} else if (v==btnSetting) {
			openSetting();
		} else if (v==btnFace) {
			startAutoCapture(Constants.TYPE_FACE);
		} else if (v==btnIncreaseSize) {
			changePreviewSize(25);
		} else if (v==btnDecreaseSize) {
			changePreviewSize(-25);
		} else if (v==btnBlack) {
			activateBlackScreen();
		}
	}

	private void activateBlackScreen() {
		isInBlackState = true;
		nonBlackState = getAllUIVisibilityState();
		hideAllUI();
		blackLayout.setVisibility(View.VISIBLE);
		if (typeCapture == Constants.TYPE_CAPTURE) {
			showToast(true, "Touch to capture", Toast.LENGTH_SHORT);
		}
	}

	private void deactivateBlackScreen() {
		isInBlackState = false;
		setAllUIVisibilityState(nonBlackState);
		ViewGroup.LayoutParams params = svPreview.getLayoutParams();
		params.width = prefs.getInt(Constants.PREFS_PREVIEW_SIZE, 250);
		params.height = getHeightBasedOnCameraRatio(params.width);
		svPreview.setLayoutParams(params);
		blackLayout.setVisibility(View.INVISIBLE);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction()==MotionEvent.ACTION_DOWN && isInBlackState && typeCapture == Constants.TYPE_CAPTURE) {
			capture();
			return true;
		}
		return super.onTouchEvent(event);
	}

	private void changePreviewSize(int size) {
		ViewGroup.LayoutParams params = svPreview.getLayoutParams();
		params.width+=size;
		if (params.width<=0) {
			params.width=1;
			params.height=1;
		} else if (params.width>(getWindowManager().getDefaultDisplay().getWidth()*3/4)) {
			return;
		} else {
			params.height=getHeightBasedOnCameraRatio(params.width);
		}
		Editor editor = prefs.edit();
		editor.putInt(Constants.PREFS_PREVIEW_SIZE, params.width);
		editor.commit();
		svPreview.setLayoutParams(params);
	}

	private int getHeightBasedOnCameraRatio(int width) {
		return width*previewSize.width/previewSize.height;
	}

	private void openSetting() {
		Intent intent = new Intent(this, SpyCamPrefsActivity.class);

		if (cameraPreviewSizes[0]!=null) {
			intent.putExtra("cameraPreviewSizes0", cameraPreviewSizes[0]);
		}
		if (cameraPreviewSizes.length>1 && cameraPreviewSizes[1]!=null) {
			intent.putExtra("cameraPreviewSizes1", cameraPreviewSizes[1]);
		}

		intent.putExtra("cameraNumber", cameraPreviewSizes.length);

		startActivityForResult(intent, ACTIVITY_SETTING);
	}

	private void startAutoFocus() {
		if (isCamConfigure) {
			lastStartAutofocus = System.currentTimeMillis();
			Log.d(TAG, "Focus mode : " + cameraParameters.getFocusMode());
			if (cameraParameters.getFocusMode().equals(Camera.Parameters.FOCUS_MODE_MACRO)) {
				camera.autoFocus(SpyCamActivity.this);
			} else {
				onAutoFocus(true, camera);
			}
		}
	}

	@SuppressLint("NewApi")
	private void startAutoCapture(int type) {
		boolean previewDisplayed = prefs.getBoolean(Constants.PREFS_PREVIEW_ON_AUTOSHOT, true);
		if (type==Constants.TYPE_AUTO) {
			if (typeCapture == Constants.TYPE_CAPTURE) {
				showToast(R.string.message_startAuto, Toast.LENGTH_SHORT);
				lastCapture = 0;
				typeCapture = Constants.TYPE_AUTO;
				btnAuto.setText(R.string.stop);
				startAutoFocus();
				if (previewDisplayed) {
					hideAllUI(btnAuto, btnBlack, svPreview, btnIncreaseSize, btnDecreaseSize);
				} else {
					hideAllUI(btnAuto, btnBlack);
				}
			} else {
				showToast(R.string.message_stopAuto, Toast.LENGTH_SHORT);
				typeCapture = Constants.TYPE_CAPTURE;
				btnAuto.setText(R.string.auto);
				camera.cancelAutoFocus();
				showAllUI();
			}
		} else if (type==Constants.TYPE_FACE) {
			if (typeCapture == Constants.TYPE_CAPTURE) {
				lastCapture = 0;
				showToast(R.string.message_startFace, Toast.LENGTH_SHORT);
				typeCapture = Constants.TYPE_FACE;
				btnFace.setText(R.string.stop);
				if (previewDisplayed) {
					hideAllUI(btnFace, btnBlack, svPreview, btnIncreaseSize, btnDecreaseSize);
				} else {
					hideAllUI(btnFace, btnBlack);
				}
				camera.setFaceDetectionListener(new FaceDetectionListener() {
					@Override
					public void onFaceDetection(Face[] faces, Camera camera) {
						Log.d(TAG, "Face detected : " + faces.length);
						long delay = 2000;
						try {
							delay = Integer.parseInt(prefs.getString(Constants.PREFS_AUTOSHOT_DELAY, "2000"));
						} catch (NumberFormatException e) {}
						if (System.currentTimeMillis()-lastCapture>delay-avgAutofocusTime) {
							startAutoFocus();
						}
					}
				});
				if (cameraParameters.getMaxNumDetectedFaces()>0) {
					camera.startFaceDetection();
				}
			} else {
				showToast(R.string.message_stopFace, Toast.LENGTH_SHORT);
				typeCapture = Constants.TYPE_CAPTURE;
				btnFace.setText(R.string.face);
				showAllUI();
				camera.setFaceDetectionListener(null);
				if (cameraParameters.getMaxNumDetectedFaces()>0) {
					camera.stopFaceDetection();
				}
			}
		}
	}

	private int[] getAllUIVisibilityState() {
		int[] state = new int[9];
		int i = 0;
		state[i++] = svPreview.getVisibility(); //1
		state[i++] = btnIncreaseSize.getVisibility();
		state[i++] = btnDecreaseSize.getVisibility();
		state[i++] = btnBlack.getVisibility(); 
		state[i++] = btnFace.getVisibility(); //5
		state[i++] = btnAuto.getVisibility();
		state[i++] = btnCapture.getVisibility();
		state[i++] = btnSetting.getVisibility();
		return state;
	}

	private void setAllUIVisibilityState(int[] state) {
		int i = 0;
		svPreview.setVisibility(state[i++]);
		btnIncreaseSize.setVisibility(state[i++]);
		btnDecreaseSize.setVisibility(state[i++]);
		btnBlack.setVisibility(state[i++]);
		btnFace.setVisibility(state[i++]);
		btnAuto.setVisibility(state[i++]);
		btnCapture.setVisibility(state[i++]);
		btnSetting.setVisibility(state[i++]);
	}

	private void showAllUI(View... views) {
		svPreview.setVisibility(View.VISIBLE);
		btnAuto.setVisibility(View.VISIBLE);
		btnCapture.setVisibility(View.VISIBLE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			btnFace.setVisibility(View.VISIBLE);
		}
		btnSetting.setVisibility(View.VISIBLE);
		btnBlack.setVisibility(View.VISIBLE);
		btnIncreaseSize.setVisibility(View.VISIBLE);
		btnDecreaseSize.setVisibility(View.VISIBLE);
		ViewGroup.LayoutParams params = svPreview.getLayoutParams();
		params.width = prefs.getInt(Constants.PREFS_PREVIEW_SIZE, 250);
		params.height = getHeightBasedOnCameraRatio(params.width);
		svPreview.setLayoutParams(params);

		if (views!=null) {
			for (View view:views) { 
				view.setVisibility(View.INVISIBLE);
			}
		}

	}

	private void hideAllUI(View... views) {
		btnAuto.setVisibility(View.INVISIBLE);
		btnCapture.setVisibility(View.INVISIBLE);
		btnFace.setVisibility(View.INVISIBLE);
		btnSetting.setVisibility(View.INVISIBLE);
		btnBlack.setVisibility(View.INVISIBLE);
		btnIncreaseSize.setVisibility(View.INVISIBLE);
		btnDecreaseSize.setVisibility(View.INVISIBLE);

		boolean isHidePreview = true;
		if (views!=null) {
			for (View view:views) { 
				view.setVisibility(View.VISIBLE);
				if (view==svPreview) {
					isHidePreview = false;
				}
			}
		}
		if (isHidePreview) {
			ViewGroup.LayoutParams params = svPreview.getLayoutParams();
			params.width=1;
			params.height=1;
			svPreview.setLayoutParams(params);
		}
	}

	private void showToast(final int message, final int lengthShort) {
		showToast(false, message, lengthShort);
	}

	private void showToast(boolean forceShow, final int message, final int lengthShort) {
		if (forceShow || (prefs.getBoolean(Constants.PREFS_SHOW_TOAST, true) && !isInBlackState)) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(SpyCamActivity.this, message, lengthShort).show();
				}
			});
		}
	}

	private void showToast(final String message, final int lengthShort) {
		showToast(false, message, lengthShort);
	}

	private void showToast(boolean forceShow, final String message, final int lengthShort) {
		Log.i(TAG, message);
		//TODO
		System.out.println(prefs.getBoolean(Constants.PREFS_SHOW_TOAST, true));
		if (forceShow || (prefs.getBoolean(Constants.PREFS_SHOW_TOAST, true) && !isInBlackState)) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(SpyCamActivity.this, message, lengthShort).show();
				}
			});
		}
	}

	private void capture() {
		typeCapture = Constants.TYPE_CAPTURE;
		startAutoFocus();
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		if (!isCamConfigure && !crashReport && camera!=null) {
			Log.i(TAG, "Configure Camera");
			isCamConfigure = true;
//			camera.stopPreview();

			int rotation = getWindowManager().getDefaultDisplay().getRotation();
			int degrees = 0;
			switch (rotation) {
			case Surface.ROTATION_0: 
				degrees = 90; 
				defaultOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
				break;
			case Surface.ROTATION_90: 
				degrees = 0; 
				defaultOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
				break;
			case Surface.ROTATION_180: 
				degrees = 270;
				if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
					defaultOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
				} else {
					defaultOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
					setRequestedOrientation(defaultOrientation);
				}
				break;
			case Surface.ROTATION_270: 
				degrees = 180; 
				if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
					defaultOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
				} else {
					defaultOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
					setRequestedOrientation(defaultOrientation);
				}
				break;
			}
			try {
				camera.setDisplayOrientation(degrees);
			} catch (RuntimeException re) {
				if (!prefs.getBoolean(Constants.PREFS_ERRORREPORT_SETDISPLAYORIENTATION, false)) {
					showFailedProcess(re
							, "Failed setting preview orientation. Sending crash report will help me fix it."
							, "Send Report"
							, "OK", false, Constants.PREFS_ERRORREPORT_SETDISPLAYORIENTATION);
				} else {
					showToast("Failed setting preview orientation.", Toast.LENGTH_LONG);
				}
			}

			if (isImageHighRes) {
				cameraParameters.setPictureSize(captureSize.width, captureSize.height);
				cameraParameters.setPictureFormat(ImageFormat.JPEG);
				cameraParameters.setRotation((cameraId==0)?90:270);
			}
			Log.d(TAG, "previewSize.width : " + previewSize.width + "x" + previewSize.height);
			cameraParameters.setPreviewSize(previewSize.width, previewSize.height);
			cameraParameters.setPreviewFormat(ImageFormat.NV21);
			camera.setParameters(cameraParameters);

			try {
				Log.d(TAG, "Starting preview");
				camera.setZoomChangeListener(this);
				camera.setPreviewDisplay(holder);
				camera.startPreview();
				camera.setPreviewCallback(this);
			} catch (IOException e) {
				Toast.makeText(this, "Failed initializing camera preview", Toast.LENGTH_LONG).show();
				Log.e(TAG, e.getMessage(), e);
			} catch (RuntimeException re) {
				if (re.getMessage().toLowerCase().contains("startpreview")) {
					clearPreference(false);
					showFailedProcess(re
							, "Failed starting camera preview. Sending crash report will help me fix it."
							, "Send Report"
							, "I've sent it", false, null);
				} else {
					throw re;
				}
			}
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Log.d(TAG, "surfaceCreated");
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.d(TAG, "surfaceDestroyed");
		if (camera != null) {
			camera.stopPreview();  
		}
	}

	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {
		//		Log.d(TAG, "onPreviewFrame");
		//Adding try catch to handle issue #5 (still not sure why it's throwing exception on black screen 
		// - possible because the surfaceview set to invisible)
		//		try {
		//			pSnapshot = camera.getParameters();
		//		} catch (Exception e) {
		//			Log.w(TAG, e);
		//		};
		bSnapShot = data;
	}

	@Override
	public void onAutoFocus(boolean isFocus, Camera camera) {
		Log.d(TAG, "Focus : " + isFocus);
		if (isFocus) {
			if (bSnapShot==null) {
				Toast.makeText(this, "Image data not found", Toast.LENGTH_SHORT).show();
				Log.w(TAG, "Image data not found");
			} else if (cameraParameters==null) {
				Toast.makeText(this, "Image parameter not found", Toast.LENGTH_SHORT).show();
				Log.w(TAG, "Image parameter not found");
			} else if (bSnapShot!=null) {
				long completeAutofocus = System.currentTimeMillis();
				synchronized (camera) {
					int delay = 2000;
					try {
						delay = Integer.parseInt(prefs.getString(Constants.PREFS_AUTOSHOT_DELAY, "2000"));
					} catch (NumberFormatException e) {}
					
//					Log.d(TAG, "lastCapture : " + lastCapture + "|" + avgAutofocusTime);
					long different = System.currentTimeMillis()-lastCapture;
					try {
						if ((different>=delay || typeCapture==Constants.TYPE_CAPTURE) && !isTakingPicture) {
							//average autofocus speed
							if (avgAutofocusTime==-1) {
								avgAutofocusTime = completeAutofocus-lastStartAutofocus;
							} else {
								avgAutofocusTime += completeAutofocus-lastStartAutofocus;
								avgAutofocusTime /= 2;
							}
							Log.d(TAG, "Average Focus Time : " + avgAutofocusTime);

							if (isImageHighRes) {
								mute();
								isTakingPicture = true;
								Log.d(TAG, "Calling takePicture");
								camera.takePicture(this, null, null, this);
								lastCapture = System.currentTimeMillis();
							} else {
								saveImage(true);
							}
						} else {
							if (isTakingPicture) {
								Log.w(TAG, "Ignoring the capture request because still in middle taking picture");
							} else {
								Log.w(TAG, "Ignoring the capture request because different time in snapshot is " + different + " (delay: "+delay+")");
							}
						}

						if (typeCapture == Constants.TYPE_CAPTURE) {
							if (isCamConfigure) {
								camera.cancelAutoFocus();
							}
						} else if (typeCapture == Constants.TYPE_AUTO) {
							long sleepTime = (delay-(System.currentTimeMillis()-lastCapture)-avgAutofocusTime);
							if (sleepTime<0) {
								sleepTime = 0;
							}
							new AutoFocusAsync().execute(sleepTime);
						}
					} catch (IOException e) {
						Log.w(TAG,e);
						if (isCamConfigure) {
							camera.cancelAutoFocus();
						}
					}
				}
			}
		} else {
			Toast.makeText(this, "Autofocus failed", Toast.LENGTH_SHORT).show();
		}
	}

	private void saveImage(boolean yuv) throws IOException {
		Log.d(TAG, "Calling saveImage");
		FileOutputStream filecon = null;
		try { 
			File directory = new File(Environment.getExternalStorageDirectory()
					.getPath() + "/SpyCamera");
			if (!directory.exists()) {
				directory.mkdir();
			}
			File file = new File( directory.getAbsolutePath()+ "/SpyPhoto_"+ SDF.format(new Date())+".jpg");
			filecon = new FileOutputStream(file);
			Size size = cameraParameters.getPreviewSize();

			//Create Image from preview data - byte[]
			// converting to RGB for rotation
			int[] rgbData = null;
			if (yuv) {
				rgbData = new int[size.width*size.height];
				decodeYUV(rgbData, bSnapShot, size.width, size.height);
				Bitmap bitmap = Bitmap.createBitmap(rgbData, size.width, size.height, Bitmap.Config.ARGB_8888);
				Matrix m = new Matrix();
				m.setRotate((cameraId==0)?90:-90, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);
				Bitmap bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, size.width, size.height, m, false);
				bitmap.recycle();
				bitmap2.compress(CompressFormat.JPEG, 100, filecon);
				bitmap2.recycle();
				showToast(file.getAbsolutePath() + " : " + bitmap.getWidth() + "x" + bitmap.getHeight(), Toast.LENGTH_SHORT);
			} else {
				filecon.write(bSnapShot);
				showToast(file.getAbsolutePath() + " : " + cameraParameters.getPictureSize().width + "x" + cameraParameters.getPictureSize().height, Toast.LENGTH_SHORT);
			}
			if (prefs.getBoolean(Constants.PREFS_VIBRATION, true)) {
				Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
				v.vibrate(Constants.VIBRATION_TIME);
			}

			/* without rotate
			YuvImage yuvSnapshot = new YuvImage(bSnapShot, pSnapshot.getPreviewFormat(),
					size.width, size.height, null);
			showToast(file.getAbsolutePath() + " : " + yuvSnapshot.getWidth() + "x" + yuvSnapshot.getHeight(), Toast.LENGTH_SHORT);
			yuvSnapshot.compressToJpeg(
					new Rect(0, 0, yuvSnapshot.getWidth(), yuvSnapshot.getHeight()), 90,
					filecon);
			 */

			//add it media scanner for Gallery
			MediaScannerConnection.scanFile(this, new String[] {file.getAbsolutePath()}, 
					new String[] {"image/jpeg"}, null);

			//			yuvSnapshot = null;
			lastCapture = System.currentTimeMillis();
		} finally {
			if (filecon!=null) {
				try {
					filecon.close();
				} catch (IOException e) {}
			}
		}
	}

	class AutoFocusAsync extends AsyncTask<Long, Void, Void> {

		@Override
		protected Void doInBackground(Long... params) {
			try {
				Log.d(TAG, "Sleep before next autofocus : " + params[0]);
				Thread.sleep(params[0]);
				while (isTakingPicture) {
					Thread.sleep(params[0]);
				}
				if (typeCapture==Constants.TYPE_AUTO || typeCapture==Constants.TYPE_FACE) {
					startAutoFocus();
				}
			} catch (InterruptedException e) {}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
		}

	}

	public static void decodeYUV(int[] out, byte[] fg, int width, int height)
			throws NullPointerException, IllegalArgumentException {
		int sz = width * height;
		if (out == null)
			throw new NullPointerException("buffer out is null");
		if (out.length < sz)
			throw new IllegalArgumentException("buffer out size " + out.length
					+ " < minimum " + sz);
		if (fg == null)
			throw new NullPointerException("buffer 'fg' is null");

		if (fg.length < sz)
			throw new IllegalArgumentException("buffer fg size " + fg.length
					+ " < minimum " + sz * 3 / 2);

		int i, j;
		int Y, Cr = 0, Cb = 0;
		for (j = 0; j < height; j++) {
			int pixPtr = j * width;
			final int jDiv2 = j >> 1;
		for (i = 0; i < width; i++) {
			Y = fg[pixPtr];
			if (Y < 0)
				Y += 255;
			if ((i & 0x1) != 1) {
				final int cOff = sz + jDiv2 * width + (i >> 1) * 2;
				Cb = fg[cOff];
				if (Cb < 0)
					Cb += 127;
				else
					Cb -= 128;
				Cr = fg[cOff + 1];
				if (Cr < 0)
					Cr += 127;
				else
					Cr -= 128;
			}
			int R = Y + Cr + (Cr >> 2) + (Cr >> 3) + (Cr >> 5);
			if (R < 0)
				R = 0;
			else if (R > 255)
				R = 255;
			int G = Y - (Cb >> 2) + (Cb >> 4) + (Cb >> 5) - (Cr >> 1)
					+ (Cr >> 3) + (Cr >> 4) + (Cr >> 5);
			if (G < 0)
				G = 0;
			else if (G > 255)
				G = 255;
			int B = Y + Cb + (Cb >> 1) + (Cb >> 2) + (Cb >> 6);
			if (B < 0)
				B = 0;
			else if (B > 255)
				B = 255;
			out[pixPtr++] = (0xff000000 + (B << 16) + (G << 8) + R);
		}
		}

	}

	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		try {
			createErrorReportFile(ex);
			
			clearPreference(true);

		} catch (Throwable ignore) {
			Log.e(TAG,
					"Error while logging uncaughtexception", ignore);
		} finally {
			camera.release();
			defaultUEH.uncaughtException(thread, ex);
		}
	}

	private void clearPreference(boolean isUncaughtExc) {
		Editor editor = prefs.edit();
		editor.clear();
		if (isUncaughtExc) {
			editor.putBoolean(Constants.PREFS_CRASHED, true);
		}
		editor.commit();
	}

	private File createErrorReportFile(Throwable ex) {
		StringBuffer info = new StringBuffer();
		info.append("cameraId: " + this.cameraId + "\n");
		info.append("defaultOrientation: " + this.defaultOrientation + "\n");
		info.append("zoomCurrent: " + this.zoomCurrent + "\n");
		info.append("isCamConfigure: " + this.isCamConfigure + "\n");
		info.append("isImageHighRes: " + this.isImageHighRes + "\n");
		info.append("isInBlackState: " + this.isInBlackState + "\n");
		info.append("isTakingPicture: " + this.isTakingPicture + "\n");
		info.append("typeCapture: " + this.typeCapture + "\n");
		info.append("previewSize: ");
		if (this.previewSize!=null) {
			info.append(this.previewSize.width + "x" + this.previewSize.height + "\n");
		} else {
			info.append("null\n");
		}
		info.append("captureSize: ");
		if (this.captureSize!=null) {
			info.append(this.captureSize.width + "x" + this.captureSize.height + "\n");
		} else {
			info.append("null\n");
		}
		info.append("cameraParameters: ");
		if (this.cameraParameters!=null) {
			info.append("\n" + "-getFocusMode(): " + this.cameraParameters.getFocusMode() + "\n");
			info.append("\n" + "-getSupportedPreviewFormats(): ");
			for (Integer i : this.cameraParameters.getSupportedPreviewFormats()) {
				info.append(i+",");
			}
			info.append("\n" + "-getSupportedPreviewSizes(): ");
			for (Camera.Size i : this.cameraParameters.getSupportedPreviewSizes()) {
				info.append(i.width+"x"+i.height+",");
			}
			info.append("\n" + "-getSupportedPictureFormats(): ");
			for (Integer i : this.cameraParameters.getSupportedPictureFormats()) {
				info.append(i+",");
			}
			info.append("\n" + "-getSupportedPictureSizes(): ");
			for (Camera.Size i : this.cameraParameters.getSupportedPictureSizes()) {
				info.append(i.width+"x"+i.height+",");
			}
		} else {
			info.append("null\n");
		}
		
		String report = Utility.createErrorReport(ex, this, info.toString());
		Log.e(TAG, "report " + report);
		File file = null;
		FileOutputStream trace = null;
		try {
			File directory = new File(Environment.getExternalStorageDirectory()
					.getPath() + "/SpyCamera");
			if (!directory.exists()) {
				directory.mkdir();
			}
			file = new File( directory.getAbsolutePath()+ "/stack.trace");
			trace = new FileOutputStream(file);
			trace.write(report.getBytes());
		} catch(IOException ioe) {
			Log.w(TAG, ioe);
		} finally {
			if (trace!=null) {
				try {
					trace.close();
				} catch (IOException e) {}
			}
		}
		return file;
	}

	private void sendEmail(File file)
	{
		BufferedReader br = null;
		try{
			br = new BufferedReader(new FileReader(file));
			String line;
			StringBuffer report = new StringBuffer();
			while ((line=br.readLine())!=null) {
				report.append(line);
				report.append("\n");
			}

			Intent sendIntent = new Intent(Intent.ACTION_SEND);
			sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			String subject = "[Spy Camera OS] Error report";
			sendIntent.putExtra(Intent.EXTRA_EMAIL,
					new String[] {"jim.halim@gmail.com"});
			sendIntent.putExtra(Intent.EXTRA_TEXT, report.toString());
			sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
			sendIntent.setType("message/rfc822");

			startActivity(Intent.createChooser(sendIntent, "Email:"));

		} catch(Exception e) {
			Log.v("sendmail", e.toString());
		} finally {
			if (br!=null) {
				try {
					br.close();
				} catch (IOException e) {}
			}
		}
	}

	private boolean checkPreviousCrash() {
		boolean justCrashed = prefs.contains(Constants.PREFS_CRASHED);
		final File file = new File(Environment.getExternalStorageDirectory().getPath() + "/SpyCamera/stack.trace");
		if (!justCrashed) {
			return false;
		}
		if (!file.exists()) {
			Toast.makeText(this, "An error was detected but no report generated", Toast.LENGTH_LONG).show();
			return false;
		}

		final AlertDialog ad = new AlertDialog.Builder(this).create();
		ad.setTitle("Error Report");
		ad.setMessage("An error was detected. Reporting it will support faster bug fixing.");
		ad.setButton(AlertDialog.BUTTON_POSITIVE, "Report it", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				sendEmail(file);
				Editor editor = prefs.edit();
				editor.remove(Constants.PREFS_CRASHED);
				editor.commit();
				ad.dismiss();
			}
		});
		ad.setButton(AlertDialog.BUTTON_NEGATIVE, "Not now", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Editor editor = prefs.edit();
				editor.remove(Constants.PREFS_CRASHED);
				editor.commit();

				Intent intent = SpyCamActivity.this.getIntent();
				SpyCamActivity.this.finish();
				SpyCamActivity.this.startActivity(intent);

				ad.dismiss();
			}
		});
		ad.show();
		return true;
	}


	@Override
	protected void finalize() throws Throwable {
		if (Thread.getDefaultUncaughtExceptionHandler().equals(this))
			Thread.setDefaultUncaughtExceptionHandler(defaultUEH);
		super.finalize();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (v==svPreview) {
			//			Log.d(TAG, "onTouch");
			scaleGestureDetector.onTouchEvent(event);
			return true;
		}
		return false;
	}

	@Override
	public boolean onScale(ScaleGestureDetector sgd) {
		Log.d(TAG, "onScale " + sgd.getScaleFactor() + "-" + cameraParameters.getMaxZoom());
		if (cameraParameters.isZoomSupported() || cameraParameters.isSmoothZoomSupported()) {
			if (sgd.getScaleFactor()>1) {
				if (cameraParameters.getZoom()<cameraParameters.getMaxZoom()) {
					zoomCurrent = cameraParameters.getZoom()+1;
					zoomRunning = true;
					if (cameraParameters.isSmoothZoomSupported()) {
						if (!zoomRunning) {
							camera.startSmoothZoom(zoomCurrent);
						}
					} else {
						cameraParameters.setZoom(zoomCurrent);
						camera.setParameters(cameraParameters);
					}
					Log.d(TAG, "Zoom to : " + zoomCurrent);
				}
			} else if (sgd.getScaleFactor()<1) {
				if (cameraParameters.getZoom()>0) {
					zoomCurrent=cameraParameters.getZoom()-1;
					zoomRunning = true;
					if (cameraParameters.isSmoothZoomSupported()) {
						if (!zoomRunning) {
							camera.startSmoothZoom(zoomCurrent);
						}
					} else {
						cameraParameters.setZoom(zoomCurrent);
						camera.setParameters(cameraParameters);
					}
					Log.d(TAG, "Zoom to : " + zoomCurrent);
				}
			}
		} else {
			if (!isZoomErrorHaveDisplayed) {
				isZoomErrorHaveDisplayed = true;
				showToast(true, "Zoom not supported", Toast.LENGTH_SHORT);
			}
		}
		return true;
	}

	@Override
	public boolean onScaleBegin(ScaleGestureDetector sgd) {
		Log.d(TAG, "onScaleBegin");
		return true;
	}

	@Override
	public void onScaleEnd(ScaleGestureDetector sgd) {
		Log.d(TAG, "onScaleEnd");
	}

	@Override
	public void onZoomChange(int zoomValue, boolean stopped, Camera camera) {
		if (stopped) {
			zoomRunning = false;
			Log.d(TAG, "Zoom stopped");
		}
	} 

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		setRequestedOrientation(defaultOrientation);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && isInBlackState) {
			deactivateBlackScreen();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onPictureTaken(byte[] data, Camera camera) {
		bSnapShot = data;
		try {
			saveImage(false);
			camera.startPreview();
			isTakingPicture = false;
		} catch (IOException e) {
			Log.w(TAG, e);
		}
	}

	@Override
	public void onShutter() {
		unmute();
	}

	private void mute() {
		Log.d(TAG, "muting..");
		AudioManager manager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		volSystem = manager.getStreamVolume(AudioManager.STREAM_SYSTEM);
		manager.setStreamVolume(AudioManager.STREAM_SYSTEM, 0 , AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
	}

	private void unmute() {
		Log.d(TAG, "unmuting..");
		AudioManager manager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		manager.setStreamVolume(AudioManager.STREAM_SYSTEM, volSystem, AudioManager.FLAG_ALLOW_RINGER_MODES);
	}

	private void showFailedProcess(final Throwable ex, String message, String captionSendReport, String captionExit
			, final boolean forceExit, final String prefsString) {
		final AlertDialog ad = new AlertDialog.Builder(this).create();
		ad.setTitle("Error Report");
		ad.setMessage(message);
		ad.setButton(AlertDialog.BUTTON_POSITIVE, captionSendReport, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (prefsString!=null) {
					Editor editor = prefs.edit();
					editor.putBoolean(prefsString, true);
					editor.commit();
				}
				sendEmail(createErrorReportFile(ex));
				ad.dismiss();
				if (forceExit) {
					finish();
				}
			}
		});
		ad.setButton(AlertDialog.BUTTON_NEGATIVE, captionExit, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				ad.dismiss();
				if (forceExit) {
					finish();
				}
			}
		});
		ad.show();
	}
	
}
