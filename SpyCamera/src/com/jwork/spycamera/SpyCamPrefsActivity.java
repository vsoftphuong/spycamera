/* ========================================================================
 * Copyright 2012 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================================
 */
package com.jwork.spycamera;

import java.io.File;
import java.io.IOException;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

/**
 * 
 * @author Jimmy
 *
 */
public class SpyCamPrefsActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener, OnPreferenceClickListener {
	
	private static final String TAG = SpyCamPrefsActivity.class.getSimpleName();
	private String[][] sizes;
	private SharedPreferences prefs;
	private Preference feedbackPlay;
	private Preference feedbackEmail;
	private Preference aboutSourceCode;
	private Preference aboutChangelog;
	private Preference generalHideFolder;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.setting);

		prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		prefs.registerOnSharedPreferenceChangeListener(this);
		
		// Set camera available
		int cameraNumber = getIntent().getIntExtra("cameraNumber", 1);
		String[] cameraPrefsOptions = new String[cameraNumber];
		String[] cameraPrefsValues = new String[cameraNumber];
		String[] cameraOptions = getResources().getStringArray(R.array.cameraOptions);
		String[] cameraValues = getResources().getStringArray(R.array.cameraValues);
		for (int i=0;i<cameraNumber;i++) {
			cameraPrefsOptions[i]=cameraOptions[i];
			cameraPrefsValues[i]=cameraValues[i];
		}
		ListPreference listPreferenceCamera = (ListPreference) findPreference("cameraId");
		listPreferenceCamera.setEntries(cameraPrefsOptions);
		listPreferenceCamera.setEntryValues(cameraPrefsValues);

		sizes = new String[cameraNumber][];
		sizes[0] = getIntent().getStringArrayExtra("cameraPreviewSizes0");
		if (sizes.length>1) {
			sizes[1] = getIntent().getStringArrayExtra("cameraPreviewSizes1");
		}
//		int activeCamera = 0;
//		try {
//			activeCamera = Integer.parseInt(prefs.getString(Constants.PREFS_CAMERA_ID, "0"));
//		} catch (NumberFormatException e) {}
//		if (sizes[activeCamera]!=null) {
//			Log.d(TAG, "Setting imageSize " + activeCamera);
		
		for (int id=0;id<sizes.length;id++) {
			ListPreference listPreferenceCategory = (ListPreference) findPreference(Constants.PREFS_IMAGE_SIZE+id);
			if (listPreferenceCategory != null) {
				CharSequence entries[] = new String[sizes[id].length];
				CharSequence entryValues[] = new String[sizes[id].length];
				int i = 0;
				for (String size : sizes[id]) {
					entries[i] = size;
					entryValues[i] = size;
					i++;
				}
				listPreferenceCategory.setEntries(entries);
				listPreferenceCategory.setEntryValues(entryValues);
			}
		}
		
		if (sizes.length==1) {
			ListPreference listPreferenceCategory = (ListPreference) findPreference(Constants.PREFS_IMAGE_SIZE_1);
			listPreferenceCategory.setEnabled(false);
		}
		
//		}

		feedbackPlay = (Preference) findPreference("feedbackPlay");
		feedbackPlay.setOnPreferenceClickListener(this);
		feedbackEmail = (Preference) findPreference("feedbackEmail");
		feedbackEmail.setOnPreferenceClickListener(this);
		aboutSourceCode = (Preference) findPreference("aboutSourceCode");
		aboutSourceCode.setOnPreferenceClickListener(this);
		aboutChangelog = (Preference) findPreference("aboutChangelog");
		aboutChangelog.setOnPreferenceClickListener(this);
		generalHideFolder = (Preference) findPreference("hideFolder");
		generalHideFolder.setOnPreferenceClickListener(this);
		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		prefs.unregisterOnSharedPreferenceChangeListener(this);
	}
	
	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		if (key.equals(Constants.PREFS_CAMERA_ID)) {
			int activeCamera = 0;
			try {
				activeCamera = Integer.parseInt(sharedPreferences.getString(Constants.PREFS_CAMERA_ID, "0"));
			} catch (NumberFormatException e) {}
			if (sizes!=null && sizes[activeCamera]!=null) {
				ListPreference listPreferenceCategory = (ListPreference) findPreference("imageSize");
				if (listPreferenceCategory != null) {
					CharSequence entries[] = new String[sizes[activeCamera].length];
					CharSequence entryValues[] = new String[sizes[activeCamera].length];
					int i = 0;
					for (String size : sizes[activeCamera]) {
						entries[i] = size;
						entryValues[i] = size;
						i++;
					}
					listPreferenceCategory.setEntries(entries);
					listPreferenceCategory.setEntryValues(entryValues);
				}
			}
		} else if (key.toLowerCase().contains(Constants.PREFS_IMAGE_SIZE.toLowerCase())) {
			String size = sharedPreferences.getString(key, "");
			if (size.endsWith("*")) {
				final AlertDialog ad = new AlertDialog.Builder(this).create();
				ad.setMessage("This resolution(that ends with *) might trigger shutter sound on some devices. If you still hear shutter sound, try to mute your system & notification volumes from Settings - Sound - Volume");
				ad.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						ad.dismiss();
					}
				});
				ad.show();
			}
		}
	}

	@Override
	public boolean onPreferenceClick(Preference preference) {
		if (preference==feedbackPlay) {
			Uri uri = Uri.parse("market://details?id=" + getPackageName());
		    Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
		    try {
		        startActivity(myAppLinkToMarket);
		    } catch (ActivityNotFoundException e) {
		        Toast.makeText(this, "Failed to find Market application", Toast.LENGTH_LONG).show();
		    }
			return true;
		} else if (preference==feedbackEmail) {
			Intent sendIntent = new Intent(Intent.ACTION_SEND);
			sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			String subject = "[Spy Camera OS] Feedback";
			sendIntent.putExtra(Intent.EXTRA_EMAIL,
					new String[] {"jim.halim@gmail.com"});
			sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
			sendIntent.setType("message/rfc822");
			startActivity(Intent.createChooser(sendIntent, "Email:"));
			return true;
		} else if (preference==aboutSourceCode) {
			Intent i = new Intent(Intent.ACTION_VIEW, 
				       Uri.parse("https://bitbucket.org/jimmod/spycamera"));
			startActivity(i);
			return true;
		} else if (preference==aboutChangelog) {
			SpyCamActivity.showChangelogNew(false, this);
			return true;
		} else if (preference==generalHideFolder) {
			File file = new File(Environment.getExternalStorageDirectory().getPath() + "/SpyCamera/.nomedia");
			if (prefs.getBoolean(Constants.PREFS_HIDE_FOLDER, false)) {
				if (!file.exists()) {
					try {
						System.out.println("Create:"+file.createNewFile());
					} catch (IOException e) {
						Log.w(TAG, e);
					}
				}
			} else {
				if (file.exists()) {
					System.out.println("Del:"+file.delete());
				}
			}
			//Rescan media
			sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
			return true;
		}
		return false;
	}
}
