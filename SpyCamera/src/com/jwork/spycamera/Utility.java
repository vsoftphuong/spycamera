package com.jwork.spycamera;

import java.util.List;

import android.app.Activity;
import android.content.pm.PackageManager.NameNotFoundException;
import android.hardware.Camera;
import android.hardware.Camera.Size;

public class Utility {
	
	public static String[] cameraSizeSupport(Camera.Parameters cameraParameters, StringBuffer data) {
		List<Size> sizes = cameraParameters.getSupportedPreviewSizes();
		List<Size> sizes2 = cameraParameters.getSupportedPictureSizes();
		String[] imageList = new String[sizes.size()+sizes2.size()];
		int n = 0;
		for (Size size:sizes2) {
			imageList[n] = size.width + "x" + size.height+"*";
			data.append(imageList[n]);
			data.append("#");
			n++;
		}
		for (Size size:sizes) {
			imageList[n] = size.width + "x" + size.height;
			data.append(imageList[n]);
			if (n!=sizes.size()+sizes2.size()) {
				data.append("#");
			}
			n++;
		}
		return imageList;
	}
	
	public static String createErrorReport(Throwable ex, Activity activity, String additionalInfo) {
		StackTraceElement[] arr = ex.getStackTrace();
		StringBuffer report = new StringBuffer();
		report.append("Please write description. ");
		report.append("Example: The app crashed when I just start the application or It crash when I press 'capture' button");
		report.append("\n\n-------------------------------\n");
		report.append(ex.toString()+"\n\n");
		report.append("--------- Stack trace ---------\n");
		for (int i=0; i<arr.length; i++)
		{
			report.append("    "+arr[i].toString()+"\n");
		}
		report.append("-------------------------------\n\n");

		report.append("--------- Cause ---------\n");
		Throwable cause = ex.getCause();
		if(cause != null) {
			report.append(cause.toString() + "\n");
			arr = cause.getStackTrace();
			for (int i=0; i<arr.length; i++)
			{
				report.append("    "+arr[i].toString()+"\n");
			}
		}
		report.append("-------------------------------\n\n");
		report.append("App version: " + activity.getString(R.string.app_versionName) + "(" + getVersion(activity) + ")" + '\n');
		report.append("Phone Model: " + android.os.Build.MODEL + '\n');
		report.append("Android Version: " + android.os.Build.VERSION.RELEASE + '\n');
		report.append("Board: " + android.os.Build.BOARD + '\n');
		report.append("Brand: " + android.os.Build.BRAND + '\n');
		report.append("Device: " + android.os.Build.DEVICE + '\n');
		report.append("Host: " + android.os.Build.HOST + '\n');
		report.append("ID: " + android.os.Build.ID + '\n');
		report.append("Model: " + android.os.Build.MODEL + '\n');
		report.append("Product: " + android.os.Build.PRODUCT + '\n');
		report.append("Type: " + android.os.Build.TYPE + '\n');
		report.append("-------------------------------\n");
		report.append(additionalInfo+"\n");
		report.append("-------------------------------\n");
		return report.toString();
	}
	
	public static int getVersion(Activity activity) {
	    int v = 0;
	    try {
	        v = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionCode;
	    } catch (NameNotFoundException e) {}
	    return v;
	}
	
}
